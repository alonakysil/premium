This project has been built using Gradle.
Google Guice is used for dependency injection.

To run unit tests:
1. Make sure you have jdk 8 or later installed.
2. Download source code locally
3. Go to the project root directory
4. Run the test task

    Mac/Linux
    
    Make sure gradlew is executable, if no run "chmod u+x gradlew" in your terminal.
    Run "./gradlew test"
    
    Windows
    
    Open Command Prompt. Run "gradlew.bat test"
    
The invocation of functionality happens in PremiumCalculator#calculate(Policy policy);


To extend implementation for new risk types, please add a risk type to RiskType enum. 
Also, either choose one of the existing handlers or, if there are no suitable, add a new one.
In managers.calculationRules.CalationRulesManagerImpl#getCalculationRules add the new risk type and a corresponding handler.
