package premium;

import com.google.inject.AbstractModule;
import premium.managers.policy.PolicyManager;
import premium.managers.policy.PolicyManagerImpl;
import premium.managers.calculationRules.CalculationRulesManager;
import premium.managers.calculationRules.CalculationRulesManagerImpl;

public class DevelopmentPremiumModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(PolicyManager.class).to(PolicyManagerImpl.class);
        bind(CalculationRulesManager.class).to(CalculationRulesManagerImpl.class);
    }

}
