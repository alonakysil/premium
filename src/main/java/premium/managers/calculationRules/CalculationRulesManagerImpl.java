package premium.managers.calculationRules;

import premium.enums.RiskType;
import premium.managers.calculationRules.handlers.Handler;
import premium.managers.calculationRules.handlers.HundredHandler;
import premium.managers.calculationRules.handlers.TenHandler;

import java.util.Map;

public class CalculationRulesManagerImpl implements CalculationRulesManager {
    @Override
    public Map<RiskType, Handler> getCalculationRules() {
        return new RulesBuilder
                .Builder()
                .addHandler(RiskType.FIRE, new HundredHandler())
                .addHandler(RiskType.WATER, new TenHandler())
                .build();
    }
}
