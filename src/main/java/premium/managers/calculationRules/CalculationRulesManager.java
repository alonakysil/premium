package premium.managers.calculationRules;

import premium.enums.RiskType;
import premium.managers.calculationRules.handlers.Handler;

import java.util.Map;

public interface CalculationRulesManager {
    Map<RiskType, Handler> getCalculationRules();
}
