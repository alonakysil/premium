package premium.managers.calculationRules;

import premium.enums.RiskType;
import premium.managers.calculationRules.handlers.BaseHandler;
import premium.managers.calculationRules.handlers.Handler;

import java.util.HashMap;
import java.util.Map;

public class RulesBuilder {

    public static class Builder {
        private Map<RiskType, Handler> rules;

        public Builder() {
            rules = new HashMap<>();
        }

        public Map<RiskType, Handler> build() {
            return new RulesBuilder(this).getRules();
        }

        public Builder addHandler(RiskType riskType, BaseHandler handler) {
            rules.put(riskType, handler);
            return this;
        }
    }

    private final Map<RiskType, Handler> rules;

    private RulesBuilder(Builder builder) {
        this.rules = builder.rules;
    }

    private Map<RiskType, Handler> getRules() {
        return rules;
    }
}
