package premium.managers.calculationRules.handlers;

import java.math.BigDecimal;

public class HundredHandler extends BaseHandler {
    @Override
    protected String getCoefficient(BigDecimal sumInsured) {
        String coefficient = "0.013";
        if (sumInsured.compareTo(new BigDecimal("100")) == 1) {
            coefficient = "0.023";
        }
        return coefficient;
    }
}
