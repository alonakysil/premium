package premium.managers.calculationRules.handlers;

import java.math.BigDecimal;

public abstract class BaseHandler implements Handler {
    @Override
    public BigDecimal calculateSubPremium(BigDecimal sumInsured) {
        String coefficient = getCoefficient(sumInsured);
        return sumInsured.multiply(new BigDecimal(coefficient));
    }

    protected abstract String getCoefficient(BigDecimal sumInsured);
}
