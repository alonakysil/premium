package premium.managers.calculationRules.handlers;

import java.math.BigDecimal;

public interface Handler {
    BigDecimal calculateSubPremium(BigDecimal sumInsured);
}
