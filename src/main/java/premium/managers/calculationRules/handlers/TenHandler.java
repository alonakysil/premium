package premium.managers.calculationRules.handlers;

import java.math.BigDecimal;

public class TenHandler extends BaseHandler {
    @Override
    protected String getCoefficient(BigDecimal sumInsured) {
        String coefficient = "0.1";
        if (sumInsured.compareTo(new BigDecimal("10")) == 1 ||
                sumInsured.compareTo(new BigDecimal("10")) == 0) {
            coefficient = "0.05";
        }
        return coefficient;
    }
}