package premium.managers.policy;

import premium.enums.RiskType;
import premium.models.Policy;

import java.math.BigDecimal;
import java.util.Map;

public interface PolicyManager {
    Map<RiskType, BigDecimal> getSumInsuredByRiskType (Policy policy);
    void validatePolicy(Policy policy);
}
