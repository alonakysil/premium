package premium.managers.policy;

import premium.enums.RiskType;
import premium.exceptions.NegativeSumInsuredException;
import premium.models.Policy;
import premium.models.PolicySubObject;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

public class PolicyManagerImpl implements PolicyManager {
    @Override
    public Map<RiskType, BigDecimal> getSumInsuredByRiskType(Policy policy) {
        return policy.getPolicyObjects()
                .stream()
                .flatMap(policyObject -> policyObject.getSubObjects().stream())
                .collect(Collectors.groupingBy(PolicySubObject::getRiskType,
                        Collectors.reducing(BigDecimal.ZERO, PolicySubObject::getSumInsured, BigDecimal::add)));
    }

    @Override
    public void validatePolicy(Policy policy) {
        if (hasNegativeValues(policy)) {
            throw new NegativeSumInsuredException();
        }
    }

    private boolean hasNegativeValues(Policy policy) {
        return policy.getPolicyObjects()
                .stream()
                .flatMap(policyObject -> policyObject.getSubObjects().stream())
                .anyMatch(subObject -> (subObject.getSumInsured().compareTo(new BigDecimal("0")) == -1));
    }
}
