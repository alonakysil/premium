package premium;

import com.google.inject.Inject;
import premium.enums.RiskType;
import premium.managers.calculationRules.CalculationRulesManager;
import premium.managers.calculationRules.handlers.Handler;
import premium.managers.policy.PolicyManager;
import premium.models.Policy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

public class PremiumCalculatorImpl implements PremiumCalculator {
    private final PolicyManager policyManager;
    private final CalculationRulesManager calculationRulesManager;

    @Inject
    PremiumCalculatorImpl(PolicyManager policyManager, CalculationRulesManager calculationRulesManager) {
        this.policyManager = policyManager;
        this.calculationRulesManager = calculationRulesManager;
    }

    @Override
    public BigDecimal calculate(Policy policy) {
        policyManager.validatePolicy(policy);

        Map<RiskType, Handler> calculationRules = calculationRulesManager.getCalculationRules();

        Map<RiskType, BigDecimal> sumInsuredByRiskType = policyManager.getSumInsuredByRiskType(policy);

        return getPremium(calculationRules, sumInsuredByRiskType);
    }

    private BigDecimal getPremium(Map<RiskType, Handler> calculationRules,
                                  Map<RiskType, BigDecimal> sumInsuredByRiskType) {
        return calculationRules.keySet()
                .stream()
                .map(riskType -> calculateSubPremium(calculationRules, sumInsuredByRiskType, riskType))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.HALF_UP);
    }

    private BigDecimal calculateSubPremium(Map<RiskType, Handler> calculationRules, Map<RiskType, BigDecimal> sumInsuredByRiskType, RiskType riskType) {
        BigDecimal sumInsured = sumInsuredByRiskType.get(riskType);
        return calculationRules.get(riskType).calculateSubPremium(sumInsured);
    }
}
