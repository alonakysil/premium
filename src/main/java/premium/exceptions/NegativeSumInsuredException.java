package premium.exceptions;

public class NegativeSumInsuredException extends RuntimeException {
    public NegativeSumInsuredException() {
        super("Sum insured should be 0 or more");
    }
}
