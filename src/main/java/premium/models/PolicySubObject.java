package premium.models;

import premium.enums.RiskType;

import java.math.BigDecimal;

public class PolicySubObject {
    private final BigDecimal sumInsured;
    private final RiskType riskType;

    public PolicySubObject(BigDecimal sumInsured, RiskType riskType) {
        this.sumInsured = sumInsured;
        this.riskType = riskType;
    }

    public BigDecimal getSumInsured() {
        if (null == sumInsured) {
            return new BigDecimal("0");
        }
        return sumInsured;
    }

    public RiskType getRiskType() {
        return riskType;
    }
}
