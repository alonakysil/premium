package premium.models;

import java.util.Collection;
import java.util.List;

public class PolicyObject {
    private final Collection<PolicySubObject> subObjects;

    public PolicyObject(List<PolicySubObject> subObjects) {
        this.subObjects = subObjects;
    }

    public Collection<PolicySubObject> getSubObjects() {
        return subObjects;
    }
}
