package premium.models;

import java.util.Collection;
import java.util.List;

public class Policy {
    private final Collection<PolicyObject> policyObjects;

    public Policy(List<PolicyObject> policyObjects) {
        this.policyObjects = policyObjects;
    }

    public Collection<PolicyObject> getPolicyObjects() {
        return policyObjects;
    }
}
