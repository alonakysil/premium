package premium.managers.calculationRules.handlers;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TenHandlerTest {
    @Test
    public void sumSmallerThanTen() {
        getCoefficient("0.1", "9");
    }

    @Test
    public void sumEqualsTen() {
        getCoefficient("0.05", "10");
    }

    @Test
    public void sumBiggerThanTen() {
        getCoefficient("0.05", "11");
    }

    private void getCoefficient(String expected, String sumInsured) {
        TenHandler tenHandler = new TenHandler();
        String actual = tenHandler.getCoefficient(new BigDecimal(sumInsured));
        assertEquals(expected, actual);
    }
}