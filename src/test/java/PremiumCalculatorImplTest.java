import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;
import premium.DevelopmentPremiumModule;
import premium.PremiumCalculatorImpl;
import premium.enums.RiskType;
import premium.exceptions.NegativeSumInsuredException;
import premium.models.Policy;
import premium.models.PolicyObject;
import premium.models.PolicySubObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PremiumCalculatorImplTest {
    @Test
    public void onePolicyObjectSmallerCoefficients() {
        PolicyObject policyObject = getPolicyObject("100", "8");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("2.10"), premium);
    }

    @Test
    public void onePolicyObjectBiggerCoefficients() {
        PolicyObject policyObject = getPolicyObject("600", "200");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("23.80"), premium);
    }

    @Test
    public void twoPolicyObjects() {
        PolicyObject policyObject1 = getPolicyObject("100", "8");
        PolicyObject policyObject2 = getPolicyObject("600", "200");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject1);
        policyObjects.add(policyObject2);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("26.50"), premium);
    }

    @Test
    public void edgeCaseFire1() {
        PolicyObject policyObject = getPolicyObject("100", "0");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("1.30"), premium);
    }

    @Test
    public void edgeCaseFire2() {
        PolicyObject policyObject = getPolicyObject("100.01", "0");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("2.30"), premium);
    }

    @Test
    public void edgeCaseWater1() {
        PolicyObject policyObject = getPolicyObject("0", "9.99");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("1.00"), premium);
    }

    @Test
    public void edgeCaseWater2() {
        PolicyObject policyObject = getPolicyObject("0", "10");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("0.50"), premium);
    }

    @Test
    public void zeroSumInsured() {
        PolicyObject policyObject = getPolicyObject("0", "0");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("0.00"), premium);
    }

    @Test(expected = NegativeSumInsuredException.class)
    public void negativeNumbers() {
        PolicyObject policyObject = getPolicyObject("-5", "-10");
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        getPremium(policyObjects);
    }

    private PolicyObject getPolicyObject(String sumInsuredFire, String sumInsuredWater) {
        PolicySubObject subObject1 = new PolicySubObject(new BigDecimal(sumInsuredFire), RiskType.FIRE);
        PolicySubObject subObject2 = new PolicySubObject(new BigDecimal(sumInsuredWater), RiskType.WATER);
        List<PolicySubObject> subObjects = new ArrayList<>();
        subObjects.add(subObject1);
        subObjects.add(subObject2);
        return new PolicyObject(subObjects);
    }

    @Test
    public void nullSumInsured() {
        PolicyObject policyObject = getPolicyObjectWithNullSumInsured();
        List<PolicyObject> policyObjects = new ArrayList<>();
        policyObjects.add(policyObject);

        BigDecimal premium = getPremium(policyObjects);

        assertEquals(new BigDecimal("0.00"), premium);
    }

    private PolicyObject getPolicyObjectWithNullSumInsured() {
        PolicySubObject subObject1 = new PolicySubObject(null, RiskType.FIRE);
        PolicySubObject subObject2 = new PolicySubObject(null, RiskType.WATER);
        List<PolicySubObject> subObjects = new ArrayList<>();
        subObjects.add(subObject1);
        subObjects.add(subObject2);
        return new PolicyObject(subObjects);
    }

    private BigDecimal getPremium(List<PolicyObject> policyObjects) {
        Policy policy = new Policy(policyObjects);

        Injector injector = Guice.createInjector(new DevelopmentPremiumModule());
        PremiumCalculatorImpl calculator = injector.getInstance(PremiumCalculatorImpl.class);
        return calculator.calculate(policy);
    }
}
